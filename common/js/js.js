// 请求时接口的公用部分
const ajax_url = "http://www.imyouz.com";

// 图片路径的公用部分
const img_src = "/static";
// 上传图片的接口
const add_img_url = "/api/common/upload";
// 上传图片的name值
const file_img_name = "file";

// 上传视频的接口
const add_video_url = "/api/common/upload";
// 上传视频的name值
const file_video_name = "img";
// 上传视频的大小限制（单位：千字节）
const video_size = 10240000;

// 网络请求
function ajax(method, url, data, success, fail, complete) {
    // let header = {
    // 	'content-type': 'application/json',
    // };
    let header = {
        'content-type': 'application/x-www-form-urlencoded',
    };
    request(header, method, url, data, success, fail, complete);
}
// 网络请求
function request(header, method, url, data, success, fail, complete) {
    uni.request({
        url: ajax_url + url,
        method: method,
        data: data,
        dataType: 'json',
        header: header,
        success: function(res) {
            if (success) {
                success(res.data);
            }
        },
        fail: function(res) {
            if (fail) {
                fail(res.data);
            }
        },
        complete: function(res) {
            if (complete) {
                complete(res.data);
            }
        }
    })
}


// 添加图片
function add_img(length, success) {
    // 获取本地图片的路径
    uni.chooseImage({
        count: length, // 默认9
        sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
        sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
        success: function(res) {
            // 显示上传动画
            show('图片上传中...');
            // 将新添加的图片添加到imgs_arr中
            var imgs = res.tempFilePaths;
            if (imgs.length > length) {
                imgs = imgs.slice(0, length);
            }
            // 调用上传图片的函数
            file_img(imgs, 0, success);
        }
    })
}
// 上传图片
function file_img(imgs, index, success) {
    // 如果数组长度大于下标，说明没有上传完
    if (imgs.length > index) {
        var src = imgs[index];
        // #ifdef H5
        lrz(src, {
            quality: 0.7,
        }).then(function(rst) { // fieldName 为 formData 中多媒体的字段名
            console.log(rst);
            //上传图片
            const UploadTask = uni.uploadFile({
                url: ajax_url + add_img_url,
                filePath: rst.base64,
                name: file_img_name,
                success: function(res) {
                    success(JSON.parse(res.data));
                    // 调用上传图片的函数
                    file_img(imgs, index + 1, success);
                },
                fail: function(e) {
                    hide();
                    ti_shi("上传超时！");
                },
            })
            UploadTask.onProgressUpdate(function(res) {
                // console.log(res);
            })
        })
        // #endif
        // #ifdef APP-PLUS
        //上传图片
        const UploadTask = uni.uploadFile({
            url: ajax_url + add_img_url,
            filePath: src,
            name: file_img_name,
            success: function(res) {
                success(JSON.parse(res.data));
                // 调用上传图片的函数
                file_img(imgs, index + 1, success);
            },
            fail: function(e) {
                hide();
                ti_shi("上传超时！");
            },
        })
        // 监听上传进度
        UploadTask.onProgressUpdate(function(res) {
            // console.log(res);
        })
        // #endif
    } else {
        hide();
    }
}
// 图片预览函数
function look_img(current, urls) {
    uni.previewImage({
        current: current, // 当前显示图片的https链接
        urls: urls // 需要预览的图片https链接列表
    })
}

// 添加视频
function add_video(success1, success2) {
    // 获取本地视频的路径
    uni.chooseVideo({
        sourceType: ['album', 'camera'],
        success: (res) => {
            console.log(res);
            if (res.size > video_size) {
                let size = parseInt(video_size / 1024000);
                ti_shi("上传视频过大，大小请不要超过" + size + "M");
            } else {
                // 显示上传动画
                show("视频上传中...");
                // 调用上传视频的函数
                file_video(res.tempFilePath, success1, success2);
            }
        },
        fail: (res) => {
            alert(JSON.stringify(res));
        },
    })
}
// 上传视频
function file_video(src, success1, success2) {
    console.log("src", src);
    //上传视频
    const UploadTask = uni.uploadFile({
        url: ajax_url + add_video_url,
        filePath: src,
        name: file_video_name,
        success: (res) => {
            success1(JSON.parse(res.data));
            // 关闭上传动画
            hide();
        },
        fail: (e) => {
            // 关闭上传动画
            hide();
            ti_shi("上传超时！");
        },
    })
    if (success2) {
        // 监听上传进度
        UploadTask.onProgressUpdate(function(res) {
            success2(res.progress);
        })
    }
}

// 显示加载动画
function show(title = "加载中...", mask = true) {
    uni.showLoading({
        title: title,
        mask: mask,
    })
}
// 关闭加载动画
function hide() {
    uni.hideLoading();
}
// 提示框
function ti_shi(title, time = 1500, mask = true, icon = "none") {
    uni.showToast({
        // 提示的内容
        title: title,
        // 提示的时间
        duration: time,
        // 是否显示透明蒙层，防止触摸穿透(false)
        mask: mask,
        // 图标(success)
        icon: icon,
    })
}
// 对话框
function dui_hua(obj) {
    let showCancel = true;
    if (obj.l_show == false) {
        showCancel = false;
    }
    uni.showModal({
        // 对话框的标题(选填)
        title: obj.title || "",
        // 对话框的内容(选填)
        content: obj.content || "",
        // 是否显示左边的按钮(选填，默认显示)
        showCancel: showCancel,
        // 左边按钮的文字内容(选填，默认取消)
        cancelText: obj.l_text || "取消",
        // 左边按钮的文字颜色(选填，默认#000000)
        cancelColor: obj.l_color || "#000000",
        // 右边按钮的文字内容(选填，默认确定)
        confirmText: obj.r_text || "确定",
        // 右边按钮的文字颜色(选填，默认#3cc51f)
        confirmColor: obj.r_color || "#3cc51f",
        success: function(res) {
            if (res.confirm) { // 点击了确定按钮
                if (obj.r_fun) {
                    obj.r_fun();
                }
            } else { // 点击了取消按钮
                if (obj.l_fun) {
                    obj.l_fun();
                }
            }
        }
    })
}

// 打开一个新页面
function open(url) {
    uni.navigateTo({
        url: url
    })
}
// 关闭所有页面，然后打开一个新页面
function open_new(url) {
    uni.reLaunch({
        url: url
    });
}
// 跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面。
function open_tab(url) {
    uni.switchTab({
        url: url
    });
}
// 当前页打开新页面
function href(url) {
    uni.redirectTo({
        url: url
    })
}
// 页面返回
function back(delta = 1) {
    uni.navigateBack({
        delta: delta
    })
}

// 将数据存到本地
function set_data(key, data) {
    uni.setStorageSync(key, data);
}
// 从本地获取数据
function get_data(key) {
    return uni.getStorageSync(key);
}
// 同步删除本地数据
function remove_data(key) {
    uni.removeStorageSync(key);
}
// 同步清除本地数据
function clear_data() {
    uni.clearStorage();
}

// 打开地图选择地址
function choose_address(success) {
    uni.chooseLocation({
        success: function(res) {
            success({
                // 地点名称
                name: res.name,
                // 详细地址
                address: res.address,
                // 经纬度
                lng: res.longitude,
                lat: res.latitude
            });
        },
    });
}
// 打开地图
function open_map(lng, lat, name) {
    uni.openLocation({
        longitude: lng - 0,
        latitude: lat - 0,
        name: name,
    })
}

// 复制
function copy(text) {
    // #ifdef APP-PLUS
    uni.setClipboardData({
        data: text,
        success: () => {
            ti_shi("复制成功");
        }
    });
    // #endif
    // #ifdef H5
    // 如果是H5，获取设备信息
    uni.getSystemInfo({
        success: function(res) {
            let input = document.createElement('input');
            input.type = 'text';
            input.value = text;
            input.style.position = 'fixed';
            input.style.top = "-100vh";
            input.style.left = "-100vw";
            input.style.opacity = 0;
            window.document.body.appendChild(input);
            input.select();
            if (res.platform == "ios") {
                input.setSelectionRange(0, input.value.length), document.execCommand('Copy');
            } else {
                document.execCommand("Copy");
            }
            document.body.removeChild(input);
            ti_shi("复制成功");
        }
    });
    // #endif
}

// 拨打电话
function call(phone) {
    uni.makePhoneCall({
        phoneNumber: phone,
    })
}

// 非空验证
function is_text(text) {
    // 是否正确(默认不正确)
    let br = false;
    if (/\S/.test(text)) {
        br = true;
    }
    return br;
}
// 手机号验证
function is_phone(phone) {
    // 是否正确(默认不正确)
    let br = false;
    if (/^0?(13[0-9]|14[5-9]|15[012356789]|166|17[0-9]|18[0-9]|19[8-9])[0-9]{8}$/.test(phone)) {
        br = true;
    }
    return br;
}
// 邮箱验证
function is_email(email) {
    // 是否正确(默认不正确)
    let br = false;
    if (/^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/.test(email)) {
        br = true;
    }
    return br;
}
// 身份证验证
function is_sfz(code) {
    //身份证号合法性验证  
    //支持15位和18位身份证号  
    //支持地址编码、出生日期、校验位验证  
    var city = {
        11: "北京",
        12: "天津",
        13: "河北",
        14: "山西",
        15: "内蒙古",
        21: "辽宁",
        22: "吉林",
        23: "黑龙江 ",
        31: "上海",
        32: "江苏",
        33: "浙江",
        34: "安徽",
        35: "福建",
        36: "江西",
        37: "山东",
        41: "河南",
        42: "湖北 ",
        43: "湖南",
        44: "广东",
        45: "广西",
        46: "海南",
        50: "重庆",
        51: "四川",
        52: "贵州",
        53: "云南",
        54: "西藏 ",
        61: "陕西",
        62: "甘肃",
        63: "青海",
        64: "宁夏",
        65: "新疆",
        71: "台湾",
        81: "香港",
        82: "澳门",
        91: "国外 "
    };
    var br = true;
    var msg = "验证成功";

    if (!code || !/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|[xX])$/.test(code)) {
        br = false;
        msg = "被保人身份证号格式错误";
    } else if (!city[code.substr(0, 2)]) {
        br = false;
        msg = "被保人身份证号地址编码错误";
    } else {
        //18位身份证需要验证最后一位校验位  
        if (code.length == 18) {
            code = code.split('');
            //∑(ai×Wi)(mod 11)  
            //加权因子  
            var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
            //校验位  
            var parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2];
            var sum = 0;
            var ai = 0;
            var wi = 0;
            for (var i = 0; i < 17; i++) {
                ai = code[i];
                wi = factor[i];
                sum += ai * wi;
            }
            if (parity[sum % 11] != code[17].toUpperCase()) {
                br = false;
                msg = "被保人身份证号校验位错误";
            }
        }
    }
    return {
        br,
        msg
    };
}

// ios输入框或文本框失去焦点时触发，页面自动回到顶部
function blur() {
    uni.getSystemInfo({
        success: function(res) {
            if (res.platform == "ios") {
                uni.pageScrollTo({
                    scrollTop: 0,
                    duration: 300
                });
            }
        }
    });
}

// 获取用户信息
function get_user_info(success) {
    let user_id = get_data("user_id") || "";
    ajax("GET", "/api/user/detail", {
        user_id: user_id,
		chat_token:1
    }, (res) => {
        if (res.code == 1) {
            success(res.data);
        } else {
            ti_shi(res.msg);
        }
    })
}


const bq_data = [{
    content: "face[微笑]",
}, {
    content: "face[嘻嘻]",
}, {
    content: "face[哈哈]",
}, {
    content: "face[可爱]",
}, {
    content: "face[可怜]",
}, {
    content: "face[挖鼻]",
}, {
    content: "face[吃惊]",
}, {
    content: "face[害羞]",
}, {
    content: "face[挤眼]",
}, {
    content: "face[闭嘴]",
}, {
    content: "face[鄙视]",
}, {
    content: "face[爱你]",
}, {
    content: "face[泪]",
}, {
    content: "face[偷笑]",
}, {
    content: "face[亲亲]",
}, {
    content: "face[生病]",
}, {
    content: "face[太开心]",
}, {
    content: "face[白眼]",
}, {
    content: "face[右哼哼]",
}, {
    content: "face[左哼哼]",
}, {
    content: "face[嘘]",
}, {
    content: "face[衰]",
}, {
    content: "face[委屈]",
}, {
    content: "face[吐]",
}, {
    content: "face[哈欠]",
}, {
    content: "face[抱抱]",
}, {
    content: "face[怒]",
}, {
    content: "face[疑问]",
}, {
    content: "face[馋嘴]",
}, {
    content: "face[拜拜]",
}, {
    content: "face[思考]",
}, {
    content: "face[汗]",
}, {
    content: "face[困]",
}, {
    content: "face[睡]",
}, {
    content: "face[钱]",
}, {
    content: "face[失望]",
}, {
    content: "face[酷]",
}, {
    content: "face[色]",
}, {
    content: "face[哼]",
}, {
    content: "face[鼓掌]",
}, {
    content: "face[晕]",
}, {
    content: "face[悲伤]",
}, {
    content: "face[抓狂]",
}, {
    content: "face[黑线]",
}, {
    content: "face[阴险]",
}, {
    content: "face[怒骂]",
}, {
    content: "face[互粉]",
}, {
    content: "face[心]",
}, {
    content: "face[伤心]",
}, {
    content: "face[猪头]",
}, {
    content: "face[熊猫]",
}, {
    content: "face[兔子]",
}, {
    content: "face[ok]",
}, {
    content: "face[耶]",
}, {
    content: "face[good]",
}, {
    content: "face[NO]",
}, {
    content: "face[赞]",
}, {
    content: "face[来]",
}, {
    content: "face[弱]",
}, {
    content: "face[草泥马]",
}, {
    content: "face[神马]",
}, {
    content: "face[囧]",
}, {
    content: "face[浮云]",
}, {
    content: "face[给力]",
}, {
    content: "face[围观]",
}, {
    content: "face[威武]",
}, {
    content: "face[奥特曼]",
}, {
    content: "face[礼物]",
}, {
    content: "face[钟]",
}, {
    content: "face[话筒]",
}, {
    content: "face[蜡烛]",
}, {
    content: "face[蛋糕]",
}];

// 动画表情
function emotion(value) {
    // 如果是图片
    if (value.indexOf("img[") != -1) {
        let src = value.split("[")[1].split("]")[0];
        if (src.substring(0, 4) != "http") {
            src = "http://chat.imyouz.com" + src;
        }
        let img = `<img style="max-width:100%;max-height:200px;" src="${src}">`
        return "<p>" + img + "</p>";
    } else {
        value = value.replace(/\n/g, "<br/>")
        const list = bq_data;
        list.forEach((obj, index) => {
            let img = `<img src="http://chat.imyouz.com/Static/asset/layui/images/face/${index}.gif">`
            let content = obj.content.split("[").join("\\[").split("]").join("\\]");
            let zheng_ze = new RegExp(content, "g");
            value = value.replace(zheng_ze, img);
        })
        return "<p>" + value + "</p>";
    }
}

module.exports = {
    // 请求的url
    ajax_url,
    // 网络请求
    ajax,

    // 图片路径的公用部分
    img_src,
    // 添加图片
    add_img,
    // 图片预览函数
    look_img,

    // 添加视频
    add_video,

    // 显示加载动画
    show,
    // 关闭加载动画
    hide,
    // 提示框
    ti_shi,
    // 对话框
    dui_hua,

    // 打开一个新页面
    open,
    // 关闭所有页面，然后打开一个新页面
    open_new,
    // 跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面。
    open_tab,
    // 当前页打开新页面
    href,
    // 页面返回
    back,

    // 将数据存到本地
    set_data,
    // 从本地获取数据
    get_data,
    // 同步删除本地数据
    remove_data,
    // 同步清除本地数据
    clear_data,

    // 打开地图选择地址
    choose_address,
    // 打开地图
    open_map,

    // 非空验证
    is_text,
    // 手机号验证
    is_phone,
    // 邮箱验证
    is_email,
    // 身份证验证
    is_sfz,

    // 复制
    copy,

    // 拨打电话
    call,

    // ios输入框或文本框失去焦点时触发，页面自动回到顶部
    blur,

    // 获取用户信息
    get_user_info,

    // 动画表情的数据
    bq_data,
    // 动画表情
    emotion,
}
