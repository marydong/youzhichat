// 微信公众号部分
// 引入微信js_sdk
import wx from '../jweixin-module';

// 微信js_sdk(通过config接口注入权限验证配置)
function js_sdk(success) {
	let url = location.href.split('#')[0];
	// 获取config参数
	ajax("POST", "/index.php?m=api&c=Common&a=js_sdk", {
		url: url,
	}, (res) => {
		wx.config({
			debug: false, // 开启调试模式
			appId: res.data.appId, // 必填，公众号的唯一标识
			timestamp: res.data.timestamp, // 必填，生成签名的时间戳
			nonceStr: res.data.nonceStr, // 必填，生成签名的随机串
			signature: res.data.signature, // 必填，签名，见附录1
			jsApiList: ["getLocation", "openCard", "onMenuShareTimeline", "onMenuShareAppMessage", "openAddress"] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
		});
		// 通过ready接口处理成功验证
		wx.ready(() => {
			if (success) {
				success(wx);
			}
		});
	})
}
// 获取用户当前位置所在的经纬度
function get_lng_lat(success) {
	js_sdk((wx) => {
		wx.getLocation({
			type: 'gcj02', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
			success: (res) => {
				success({
					root: true,
					lng: res.longitude,
					lat: res.latitude,
				});
			},
			fail: (res) => {
				success({
					root: false,
				});
			},
			cancel: (res) => {
				success({
					root: false,
				});
			}
		});
	})
}
// 获取收获地址
function get_shdz(success) {
	js_sdk((wx) => {
		wx.openAddress({
			success: (res) => {
				success(res);
			}
		});
	})
}
// 分享
function fen_xiang(obj = {
    title: "标题",
    content: "内容",
    img: "",
    pages: "",
}) {
    js_sdk((wx) => {
        // 域名
        let url = location.href.split('/#/')[0];
        let fx_title = obj.title,
            fx_desc = obj.content,
            fx_url = url + '/redirect.html?app3Redirect=' + encodeURIComponent(url + "/#" + obj.pages),
            fx_img = obj.img;

        console.log({
            title: fx_title, // 分享标题
            link: fx_url, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
            imgUrl: fx_img, // 分享图标
        })
        wx.onMenuShareTimeline({
            title: fx_title, // 分享标题
            link: fx_url, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
            imgUrl: fx_img, // 分享图标
        });
        wx.onMenuShareAppMessage({
            title: fx_title, // 分享标题
            desc: fx_desc, // 分享描述
            link: fx_url, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
            imgUrl: fx_img, // 分享图标
        });
    })
}

module.exports = {
	// 获取用户当前位置所在的经纬度
	get_lng_lat,
	// 获取收获地址
	get_shdz,
	// 分享
	fen_xiang,
}
