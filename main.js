import Vue from 'vue'
//#ifdef H5
// 图片上传压缩
import lrz from './common/lrz'
//#endif
// 方法的快捷调用
import $ from './common/js/js.js'
Vue.prototype.$ = $

import App from './App'

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
//http://imyouz.com/chat/#/?user_id=3&tlz_id=10014&token=5e20532dd585e5e20532dd58613

//http://imyouz.com/chat/#/?user_id=4&tlz_id=10014&token=5e2023cb8c1435e2023cb8c1473

//http://localhost:8080/chat/#/pages/tlz/xiangqing/?user_id=2&tlz_id=10014&token=5e4bcdc8500305e4bcdc85003422
//http://localhost:8080/chat/#/?user_id=2&tlz_id=10014&token=5e205309ea2d25e205309ea2d52
